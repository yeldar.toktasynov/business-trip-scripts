const { Client, logger } = require('camunda-external-task-client-js')
const { Variables } = require('camunda-external-task-client-js')
const BearerTokenInterceptor = require('camunda-external-task-bearer-token-interceptor')
const nodemailer = require('nodemailer')
const moment = require('moment')

// ################ Keycloak ##########################
let bearerTokenInterceptor = null
if (process.env.KEYCLOAK_CLIENT_ID && process.env.KEYCLOAK_CLIENT_SECRET && process.env.KEYCLOAK_BASE_URL && process.env.KEYCLOAK_REALM_ID) {
  bearerTokenInterceptor = new BearerTokenInterceptor({
    clientId: process.env.KEYCLOAK_CLIENT_ID,
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
    baseUrl: process.env.KEYCLOAK_BASE_URL,
    readlmId: process.env.KEYCLOAK_REALM_ID,
  })
}

// ################ Camunda ##########################

const config = { baseUrl: process.env.CAMUNDA_URL, use: logger, interceptors: bearerTokenInterceptor, workerId: 'workflow-messages', interval: 1000 }

// create a Client instance with custom configuration
const client = new Client(config)

// ################ nodemailer #######################
// create reusable transporter object using the default SMTP transport

const GoogleSpreadsheet = require('google-spreadsheet')
const creds = require('./client_secret.json')
const doc = new GoogleSpreadsheet('1Nw2m2UYuHiHKnBWgIH7Z5HPfw2WQVnb2EDqD-6xmRCg')
//---------------------------------------------------------------------------------------------------------------------------
// Конвертация excel файла в JSON
client.subscribe('search-in-excel', ({ task, taskService }) => {
  let empl = {}
  let count = 0;
  let x = 0;
  const typedValues = task.variables.getAllTyped()
  const user_id = typedValues.starter.value
  //const coord_id = typedValues.coordinator.value
  //console.log(`Starter is: ${empl}`)
  // Create a document object using the ID of the spreadsheet - obtained from its URL.


  doc.useServiceAccountAuth(creds, (err) => {
    if (err) {
      taskService.handleFailure(task, 'Authentication Error').then((result) => {
        console.log(`Result: ${result}`)
      })
    } else {
      // Get all of the rows from the spreadsheet.
      doc.getRows(1, (err, rows) => {
        rows.forEach((element) => {
          if (element.userid === user_id) {
            empl.userid = element.userid
            empl.fio = element.fio
            empl.phone = element.phone
            empl.email = element.email
            empl.position = element.position
            empl.originplace = element.originplace
            empl.coordinatorphone = element.coordinatorphone
            empl.linemanager = element.linemanager
            count = x
          }
          x += 1
        })
        const variables = new Variables().setAllTyped({
          empl: {
            value: empl,
            type: 'Json',
          },
          count: {
            value: count,
            type: 'Long',
          },
          // coord_id: {
          //   value: coord_id,
          //   type: 'String',
          // },
        })
        taskService.complete(task, variables).then((result) => {
          console.log(`Result: ${result}`)
        })
      })
    }
  })
})

// Конвертация excel файла в JSON
client.subscribe('upload-to-gsh', async function ({ task, taskService }) {
  const typedValues = task.variables.getAllTyped();
  let count = typedValues.count.value
  let tripFrom = typedValues.tripFromGsh.value
  let tripTill = typedValues.tripTillGsh.value
  // console.log(tripFrom.substring(0, ))
  // console.log(moment(tripTill, 'DD.MM.YYYY').toDate())
  var async = require('async');
  var sheet;

  async.series([function setAuth(step) {
    var creds_json = {
      client_email: "tojepoh@business-trip-213310.iam.gserviceaccount.com",
      private_key: "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCamGRd8n8jOWpl\nJonuBAeY35Ap2WIs+5iy7B/ZUnT+vsrB2yU6hsC/IS1UsERd+qnqBhF9z4jq4FQJ\nbW9EcxM2lcPx8d0+bRjXhxWzGFfye3GsP0u38RVmfx1nbYx1XQUspcnQaIosCelr\nLFbOcyJv3etHGQIS0f/0gZls43za4ZpZNM+AU+Poc7GUpSZNtR5exIAP96KPKKLX\nGVe25yBINqd2AAzoFphtXd4IzhbjFGX7rBAT0+SNyQB1aCrYt6174MhF6dQlILCh\n5p5UekpFN9EDOtRwjiQDkvV7hj6ZA+5HXWUcxAMtrfpZhMPQt54xdCToPbyUmzTL\nfPMFW1lRAgMBAAECggEAF4OQBIrH/ri9b5bYijeFBky+1eFcIZZqcpB1Wa3v4KLE\nHo9GuYEEhoMKgjlF9NDAZmQ3kIjFUmBTjT7exS0G95mjp5m6T+6dGtnPmaGIGE2M\nC9RxdRAPdBBn4wg9Fx7Saijp/4U0ZaAOUlWjUHHKWPSKqZJAB/0hWXRwPH6EykUj\nXnDPTvNTP29CDyZl0KLvHe/+fPZLyp93FgK9Ugzcc+NzizV/piZQRGmdI4O+eh1i\nbF8bsEql1S2TUj0kRWPrJLcNTAeab3GTLljM90m0n15pEVqKkzBFsysiJxnKuvnZ\nt1QQDv/mGaxaWQFTuDAvXFTsTFIMRljVgskndOL3pwKBgQDQx4zS+iOc2dn81B7E\nn3JzJ7p+IIwNiVGP0qCCS2hsZkQDDVbmynSe5JNMXQvWGrnzHeZkw1EyLsFGygPS\nZk4b1CQaiTnhBdnt2Bnn2c4tupGooWlrEKLqyb7b16drSFWy8A12Caj0v+fw9Knz\nA0MEdSWhyB3EPySmCDpv6eLCxwKBgQC9j4uzPA8eCV6nwmr9FIKdOMP2ZY2kd9sz\nDCn03TI8NBj/6b+9WPZ6eOsyRdOfaSIaGWvmv7mshkdOBDAF3/7u5MglL2FbGiPJ\nps8Jm9cVabp9omwEggp9WkBxdzhGafU6dYG/HdvfVc4eCpz2w5rNBqBt7FiKTH0j\n30huCeXrJwKBgQCHiNP57psJ6z8ha88hk+UnrqXZ1WU2MGv7cx/yFeQPYwBJ/vHo\nV4wHzpl1HH5fmvAWTNXRmh5u9n1QaDq87t9MH5bkhytE0picWArRQcsgUprWnxqh\nf6ZTx26Yp1IvSkEgLch/VVwSrzZybVKayQZLsYUhlpkEWn1/L55IIsyr5wKBgDMY\nOx6fxK9i9Smg5Pe8jIxWXbPPocNAL0xuLez7pmvF2ys31V/zV9ShPTw6yBUC4JdK\nXORAr4qKUzP7OvqKawv3jN9BCyvkOttC5DGB2RLDgeoVWqpIZw1sw0wSR4/rRZdx\nTvy1zqs/VKSSVcbJ6LL0409d0mvMvDpsF/jZICknAoGBAKiQEhuvYQT8I5FIyJJj\nJbeNYKAi9XohOp1lQOiVNWK7VDEuuC4N5b2VPK8Ibc9LSBkUt8fZ7SqPLhRk1nVB\nh6oc9IArjUkO+j8XJwnHswR1ZFYBqMsZBuUYmo/MOTbYnlAEQFqansJzvgAuqtD4\nnACYUOpsyruTUbHoVB4UHoXE\n-----END PRIVATE KEY-----\n"
    }

    doc.useServiceAccountAuth(creds, step);
  },
  function getInfoAndWorksheets(step) {
    doc.getInfo(function (err, info) {
      console.log('Loaded doc: ' + info.title + ' by ' + info.author.email);
      sheet = info.worksheets[0];
      console.log('sheet 1: ' + sheet.title + ' ' + sheet.rowCount + 'x' + sheet.colCount);
      step();
    });
  },
  function workingWithRows(step) {
    sheet.getRows({
      offset: 1,
      // query: typedValues.empl.tabno
    }, function (err, rows) {
      console.log(`Read ${rows.length} rows`);
      rows[count].tripFrom = tripFrom,
      rows[count].tripTill = tripTill,
      rows[count].save();
      step();
    });
  },
  ], function (err) {
    if (err) {
      console.log('Error: ' + err);
    }
  })
  await taskService.complete(task)
})

client.subscribe('send-sms', async({ task, taskService }) => {
  await taskService.complete(task)
})